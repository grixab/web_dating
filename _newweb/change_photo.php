<?php
session_start();

// Подключение к базе данных
$conn = new mysqli("localhost", "root", "", "dating");

// Проверка соединения
if ($conn->connect_error) {
    die("Ошибка подключения к базе данных: " . $conn->connect_error);
}

// Получение информации о текущем пользователе
$userID = $_SESSION['userID'];
$sql_user = "SELECT * FROM users WHERE id = $userID";
$result_user = $conn->query($sql_user);
$user = $result_user->fetch_assoc();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $avatar = $_FILES['avatar']['name'];
    $avatar_tmp = $_FILES['avatar']['tmp_name'];
    $avatar_path = "avatars/" . $avatar;

    move_uploaded_file($avatar_tmp, $avatar_path);

    // Обновление пути к новой фотографии пользователя
    $sql_update = "UPDATE users SET avatar = '$avatar_path' WHERE id = $userID";

    if ($conn->query($sql_update) === TRUE) {
        $_SESSION["success_message"] = "Фотография успешно изменена.";
        $user['avatar'] = $avatar_path; // Обновляем путь к фотографии в текущей сессии
    } else {
        $_SESSION["error_message"] = "Ошибка при изменении фотографии: " . $conn->error;
    }

    header("Location: change_photo.php");
    exit();
}

// Закрытие соединения с базой данных
$conn->close();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="img/logo.svg">
    <title>Профиль пользователя</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <style>
        .profile {
            text-align: center;
            margin-top: 50px;
        }
        .profile-image {
            width: 250px;
            height: 250px;
            border-radius: 50%;
            object-fit: cover;
            margin-bottom: 20px;
        }
        .profile-name {
            font-size: 24px;
            margin-bottom: 10px;
        }
        .profile-details {
            font-size: 18px;
            margin-bottom: 10px;
        }
        .edit-profile-button {
            margin-top: 20px;
            color: white;
        }
        
    </style>
</head>
<body>
    <header>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
						<img src="img/logo.svg" alt="Logo" width="25">
						Сменить фото
					</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item">
                        <a class="nav-link" href="svape.php">Свайпы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="favorites.php">Понравившиеся</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="profile.php">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Выход</a>
                    </li>
                </ul>
            </div>
        </nav>
            </div></div></div>
    </header>


    <div class="container">
        <?php if (isset($_SESSION['success_message'])): ?>
            <p class="success-message"><?php echo $_SESSION['success_message']; ?></p>
            <?php unset($_SESSION['success_message']); ?>
        <?php endif; ?>

        <?php if (isset($_SESSION['error_message'])): ?>
            <p class="error-message"><?php echo $_SESSION['error_message']; ?></p>
            <?php unset($_SESSION['error_message']); ?>
        <?php endif; ?>

        <div class="avatar-container">
            <img class = "profile-image" src="<?php echo $user['avatar']; ?>" alt="Avatar">
        </div>

        <form action="change_photo.php" method="post" enctype="multipart/form-data">
            <input type="file" name="avatar" accept="image/*" required>
            <button type="submit" class="btn btn-primary">Изменить фотографию</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>