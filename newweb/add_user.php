<?php
// Подключение к базе данных MySQL
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dating";

$conn = new mysqli($servername, $username, $password, $dbname);

// Проверка соединения
if ($conn->connect_error) {
    die("Ошибка подключения к базе данных: " . $conn->connect_error);
}

// Получение данных из формы

$username = $_POST['username'];
$login = $_POST['login'];
$email = $_POST['email'];
$age = $_POST['age'];
$city = $_POST['city'];
$gender = $_POST['gender'];
$password = $_POST['password'];

// Обработка загруженного аватара
$avatar = $_FILES['avatar'];
$avatarName = $avatar['name'];
$avatarTmpName = $avatar['tmp_name'];
$avatarSize = $avatar['size'];

// Переместить загруженный файл в папку для аватаров
$avatarDestination = "avatars/" . $avatarName;
move_uploaded_file($avatarTmpName, $avatarDestination);

// Вставка данных в таблицу пользователей
$sql = "INSERT INTO users (username, login, email, age, city, gender, password, avatar)
        VALUES ('$username', '$login', '$email', '$age', '$city', '$gender', '$password', '$avatarDestination')";

if ($conn->query($sql) === TRUE) {
    $_SESSION["success_message_add"] = "Данные пользователя успешно обновлены.";
} else {
    echo "Ошибка: " . $sql . "<br>" . $conn->error;
}
header("Location: admin_panel.php");
$conn->close();

//exit();
?>