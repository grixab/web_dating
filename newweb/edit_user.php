<?php
session_start();

// Подключение к базе данных
$conn = new mysqli("localhost", "root", "", "dating");

// Проверка соединения
if ($conn->connect_error) {
    die("Ошибка подключения к базе данных: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST["userID"];
    $username = $_POST["username"];
    $login = $_POST["login"];
    $email = $_POST["email"];
    $age = $_POST["age"];
    $city = $_POST["city"];
    $gender = $_POST["gender"];
    
    // Обновление данных пользователя
    $sql = "UPDATE users SET username='$username', login='$login', email='$email', age=$age, city='$city', gender='$gender' WHERE id=$id";

    if ($conn->query($sql) === TRUE) {
        $_SESSION["success_message_update"] = "Данные пользователя успешно обновлены.";
    } else {
        $_SESSION["error_message_update"] = "Ошибка при обновлении данных пользователя: " . $conn->error;
    }

    header("Location: admin_panel.php");
    exit();
}

// Закрытие соединения с базой данных
$conn->close();
?>
